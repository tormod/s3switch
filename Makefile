CFLAGS = -g
CC = gcc

all: s3switch

s3switch: s3switch.o lrmi.o

clean:
	rm -f s3switch *.o
