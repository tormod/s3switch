s3switch (0.1-2) UNRELEASED; urgency=medium

  * debian/control: Update Vcs links to salsa
  * debian/rules: Use .install file for binary (Closes: #968529)

 -- Tormod Volden <debian.tormod@gmail.com>  Tue, 08 Sep 2020 21:53:16 +0200

s3switch (0.1-1) unstable; urgency=low

  * New upstream release and upstream location
    - Fixes building on newer kernels (Closes: #310984, LP: #41355)
  * debian/copyright: Updated and made machine-interpretable
  * Switch to 3.0 (quilt) source format
  * debian/control: Bump Standards-Version to 3.9.2
  * debian/control: Make myself maintainer
  * debian/rules: Add build-arch and build-indep targets
  * debian/watch added

 -- Tormod Volden <debian.tormod@gmail.com>  Mon, 21 Nov 2011 22:33:36 +0100

s3switch (0.0.20030423-2) unstable; urgency=low

  * add patch from
    http://lists.snowman.net/pipermail/mythtv-users/2003-April/003032.html to
    let TVout work on ProSavageDDR. Thanks to  Laurent Martelli <laurent at
    bearteam.org> for spotting this. (Closes: #207878)
  * bump Standards-Version to 3.6.1

 -- Guido Guenther <agx@debian.org>  Sun, 26 Oct 2003 22:15:02 +0100

s3switch (0.0.20030423-1) unstable; urgency=low

  * new upstream version
  * bump standards version to 3.5.9
  * echo 4 > debian/compat
  * cleanup debian/copyright
  * honor DEB_BUILD_OPTIONS
  * add ${misc:Depends}
  * build-depend on debhelper >= 4
  * fix includes in s3switch.c to kill gcc warnings

 -- Guido Guenther <agx@debian.org>  Wed, 23 Apr 2003 23:27:30 +0200

s3switch (0.0.20020501-1) unstable; urgency=low

  * new upstream version (Closes: #144497)

 -- Guido Guenther <agx@debian.org>  Wed,  1 May 2002 15:27:21 +0200

s3switch (0.0.20010719-4) unstable; urgency=low

  * fix spelling in control file (Closes: #125327)

 -- Guido Guenther <agx@debian.org>  Fri, 21 Dec 2001 21:38:02 +0100

s3switch (0.0.20010719-3) unstable; urgency=low

  * add support for the SuperSavage chips - thanks to Tim Roberts
  * no need to ship two almost identical versions of the
    manpage in the source package

 -- Guido Guenther <agx@debian.org>  Sun, 16 Dec 2001 02:39:42 +0100

s3switch (0.0.20010719-2) unstable; urgency=low

  * added support for the ProSavage PN(0x8d01) and KN(0x8d02) chips
    (Closes: #119379)
  * fixed spelling of package description according to:
    http://people.debian.org/~mdz/spelling/corrections.diff.gz

 -- Guido Guenther <agx@debian.org>  Wed, 21 Nov 2001 00:12:44 +0100

s3switch (0.0.20010719-1) unstable; urgency=low

  * arch is i386 only (Closes: #105865)
  * changed priority to extra
  * properly split out upstream ''package'' to clarify, that this
    is not a debian native package

 -- Guido Guenther <agx@debian.org>  Thu, 19 Jul 2001 21:03:03 +0200

s3switch (0.0.20010717) unstable; urgency=low

  * initial release (Closes: #103413)
  * moved s3switch.1x manpage to s3switch.8 since it's not a X
    application
  * added proper copyright notice
  * added clean rule to Makefile

 -- Guido Guenther <agx@debian.org>  Sun,  8 Jul 2001 00:23:33 +0200

